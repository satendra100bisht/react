import React, { Component } from "react";
import Cell from "./cell1";
class MainComponent extends Component {
  state = {
    arrayCell: [
      { image: "/animals/croc.svg", val: -1, cell: 1, open: false },
      { image: "/animals/elephant.svg", val: -1, cell: 2, open: false },
      { image: "/animals/giraffe.svg", val: -1, cell: 3, open: false },
      { image: "/animals/gorilla.svg", val: -1, cell: 4, open: false },
      { image: "/animals/koala.svg", val: -1, cell: 5, open: false },
      { image: "/animals/polar-bear.svg", val: -1, cell: 6, open: false },
      { image: "/animals/tiger.svg", val: -1, cell: 7, open: false },
      { image: "/animals/whale.svg", val: -1, cell: 8, open: false },
      { image: "/animals/croc.svg", val: -1, cell: 9, open: false },
      { image: "/animals/elephant.svg", val: -1, cell: 10, open: false },
      { image: "/animals/giraffe.svg", val: -1, cell: 11, open: false },
      { image: "/animals/gorilla.svg", val: -1, cell: 12, open: false },
      { image: "/animals/koala.svg", val: -1, cell: 13, open: false },
      { image: "/animals/polar-bear.svg", val: -1, cell: 14, open: false },
      { image: "/animals/tiger.svg", val: -1, cell: 15, open: false },
      { image: "/animals/whale.svg", val: -1, cell: 16, open: false }
    ],
    arrayCell1: [
      { image: "/animals/croc.svg", val: -1, cell: 1, open: false },
      { image: "/animals/elephant.svg", val: -1, cell: 2, open: false },
      { image: "/animals/giraffe.svg", val: -1, cell: 3, open: false },
      { image: "/animals/gorilla.svg", val: -1, cell: 4, open: false },
      { image: "/animals/koala.svg", val: -1, cell: 5, open: false },
      { image: "/animals/polar-bear.svg", val: -1, cell: 6, open: false },
      { image: "/animals/tiger.svg", val: -1, cell: 7, open: false },
      { image: "/animals/whale.svg", val: -1, cell: 8, open: false },
      { image: "/animals/croc.svg", val: -1, cell: 9, open: false },
      { image: "/animals/elephant.svg", val: -1, cell: 10, open: false },
      { image: "/animals/giraffe.svg", val: -1, cell: 11, open: false },
      { image: "/animals/gorilla.svg", val: -1, cell: 12, open: false },
      { image: "/animals/koala.svg", val: -1, cell: 13, open: false },
      { image: "/animals/polar-bear.svg", val: -1, cell: 14, open: false },
      { image: "/animals/tiger.svg", val: -1, cell: 15, open: false },
      { image: "/animals/whale.svg", val: -1, cell: 16, open: false }
    ],
    open1: -1,
    open2: -1
  };
  handleClick = val => {
    let newarr = this.state.arrayCell;
    let ind = newarr.findIndex(n1 => n1.cell === val.cell);
    newarr[ind].val = 1;
    newarr[ind].open = true;
    let open1 = this.state.open1;
    let open2 = this.state.open2;
    if (open1 === -1) open1 = val;
    else open2 = val;

    this.setState({ arrayCell: newarr, open1, open2 });
    if (open2 !== -1) setTimeout(() => this.doCellsMatching(), 1000);
  };
  doCellsMatching() {
    console.log(this.state.open1, this.state.open2);
    let newarr = this.state.arrayCell;
    let ind1 = newarr.findIndex(n1 => n1.cell === this.state.open1.cell);
    let ind2 = newarr.findIndex(n1 => n1.cell === this.state.open2.cell);
    if (this.state.open1.image === this.state.open2.image) {
      newarr[ind1].image = "";
      newarr[ind2].image = "";
      this.setState({ arrayCell: newarr, open1: -1, open2: -1 });
    } else {
      newarr[ind1].val = -1;
      newarr[ind1].open = false;
      newarr[ind2].val = -1;
      newarr[ind2].open = false;

      this.setState({ open1: -1, open2: -1, arrayCell: newarr });
    }
  }
  handleGame = () => {
    let arrayCell = this.state.arrayCell1;
    this.setState({ arrayCell: arrayCell, open1: -1, open2: -1 });
  };
  render() {
    let animals = this.state.arrayCell;

    return (
      <div className="container">
        <br />

        <div className="row">
          <Cell value={animals[0]} onhandleClick={this.handleClick} />
          <Cell value={animals[1]} onhandleClick={this.handleClick} />
          <Cell value={animals[2]} onhandleClick={this.handleClick} />
          <Cell value={animals[3]} onhandleClick={this.handleClick} />
        </div>

        <div className="row">
          <Cell value={animals[4]} onhandleClick={this.handleClick} />
          <Cell value={animals[5]} onhandleClick={this.handleClick} />
          <Cell value={animals[6]} onhandleClick={this.handleClick} />
          <Cell value={animals[7]} onhandleClick={this.handleClick} />
        </div>

        <div className="row">
          <Cell value={animals[8]} onhandleClick={this.handleClick} />
          <Cell value={animals[9]} onhandleClick={this.handleClick} />
          <Cell value={animals[10]} onhandleClick={this.handleClick} />
          <Cell value={animals[11]} onhandleClick={this.handleClick} />
        </div>

        <div className="row">
          <Cell value={animals[12]} onhandleClick={this.handleClick} />
          <Cell value={animals[13]} onhandleClick={this.handleClick} />
          <Cell value={animals[14]} onhandleClick={this.handleClick} />
          <Cell value={animals[15]} onhandleClick={this.handleClick} />
        </div>
        <button className="btn btn-primary" onClick={() => this.handleGame()}>
          Reset Game
        </button>
      </div>
    );
  }
}

export default MainComponent;
