import React, { Component } from "react";
import axios from "axios";
import config from "./config.json";

class Books extends Component {
  state = {
    books: ""
  };
  addTOParams(params, newParamName, newParamValue) {
    if (newParamValue) {
      params = params ? params + "&" : params + "?";
      params = params + newParamName + "=" + newParamValue;
    }
    return params;
  }
  calUrl = params => {
    let path = "/books";
    const boks = this.props.match.params.boks;
    if (boks) path = path + "/" + boks;
    params = this.addTOParams(params);

    this.props.history.push({
      pathname: path,
      search: params
    });
  };
  async componentDidMount() {
    let params = "";
    this.calUrl(params);

    let boks = this.props.match.params.boks;
    console.log(boks);
    params = this.addTOParams(params);
    let bookurl = "";
    if ((boks = "books")) {
      bookurl = config.apiBook;
    } else {
      bookurl = config.apiBook + "/" + boks;
    }

    const { data: books } = await axios.get(bookurl);

    this.setState({ books });
    console.log(this.state.books);
  }

  /*async componentDidUpdate(prevProps, prevState) {
    console.log("previousprops", prevProps);
    console.log("this.props", this.props);
    console.log(prevState);
    if (this.props.location.key === prevProps.location.key) {
      let params = "";
      this.calUrl(params);
      let boks = this.props.match.params.boks;
      console.log(boks);
      params = this.addTOParams(params);
      let bookurl = "";
      if ((boks = "books")) {
        bookurl = config.apiBook;
      } else {
        bookurl = config.apiBook + "/" + boks;
      }

      const { data: books } = await axios.get(bookurl);

      this.setState({ books });
      console.log(this.state.books);
    }
  }*/

  render() {
    let boks = this.props.match.params.boks;

    return (
      <div className="container">
        <div className="row">
          <div className="col-2">Left Panel</div>
          <div className="col-10">
            <div className="row">
              <div className="col-4 border bg-primary  text-center">
                {"Title"}
              </div>
              <div className="col-3 border bg-primary  text-center">
                {"Author"}
              </div>
              <div className="col-2 border bg-primary  text-center">
                {"Language"}
              </div>
              <div className="col-1 border bg-primary  text-center">
                {"Genre"}
              </div>
              <div className="col-1 border bg-primary  text-center">
                {"Price"}
              </div>
              <div className="col-1 border bg-primary  text-center">
                {"Bests..."}
              </div>
            </div>
            {this.state.books !== "" ? (
              <React.Fragment>
                {this.state.books.data.map(buk => (
                  <div className="row" key={buk.name}>
                    <div className="col-4 border">{buk.name}</div>
                    <div className="col-3 border">{buk.author}</div>
                    <div className="col-2 border">{buk.language}</div>
                    <div className="col-1 border">{buk.genre}</div>
                    <div className="col-1 border">{buk.price}</div>
                    <div className="col-1 border">{buk.bestseller}</div>
                  </div>
                ))}
              </React.Fragment>
            ) : (
              ""
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default Books;
