import React, { Component } from "react";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import NavBar from "./navbar";
import Books from "./book";

class Main extends Component {
  render() {
    return (
      <BrowserRouter>
        <div>
          <NavBar />
          <div>
            <Switch>
              <Route path="/:boks" component={Books} />
              <Redirect to="/books" />
            </Switch>
          </div>
        </div>
      </BrowserRouter>
    );
  }
}

export default Main;
